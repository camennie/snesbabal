;Stuff mostly related to what's going on, on BG1 during 

SetUpTimeLeft:
;{{{
	php
	pha
	phx
	phy
	
	phx		;Give us a bit of stack

	;Get # of lines in level
	rep #$30
	lda $CurrentLevelPointer
	sta 0,s
	sep #$20

	ldy #$0000
	lda (0,s),y
	
	;Set time left relative to number of lines
	lsr
	sta TimeLeft	
	
	plx		;Restore stack
	
	ply
	plx
	pla
	plp
	rts
;}}}


SetUpMagicLeft:
;{{{
	php
	pha
	phx
	phy

	phx		;Give us a bit of stack

	;Get # of lines in level
	rep #$30
	lda $CurrentLevelPointer
	sta 0,s
	sep #$20

	ldy #$0000
	lda (0,s),y
	
	;Set magic left relative to number of lines
	ldx #$0000
	stx MagicLeft
	sta MagicLeft
	
	plx		;Restore stack

	
	ply
	plx
	pla
	plp

	rts
	;}}}
	

ScrollBG1:
;{{{
	; Purpose: Scroll BG1 up one pixel. If an entire block is now hidden above, draw the next block hidden below
	; Returns (in A) 0 if more to scroll, or 1 if entire level has scrolled by

	;Set up
	nop
	nop
	phx
	phy
	php
	
	
	;Give us 2 bytes of stack
	rep #$30
	tsc
	sec
	sbc #$02
	tcs
	lda #$0000	;The high byte of A should become 0 on the rep below, but there's no guarantee
	sep #$20

	
	;Scroll BG1 up	
	lda AmountBG1Scrolled
	inc
	and #$ff	;Is this necessary? Probably not...
	sta AmountBG1Scrolled
	sta $210e
	stz $210e

	;Check to see if we just moved an entire block out of view
	inc		;No scroll is $ff instead of $00, so we do this..
	and #$1f
	beq DoBScroll1

	lda #$00
	bra ScrollBG1TearDown
	
	;If so, then draw a new block below the current display
DoBScroll1:
		nop
		lda ScreenTopRow
		inc
		and #$07
		sta ScreenTopRow

		lda ScreenTopLevelRow
		inc
		sta ScreenTopLevelRow


		;If A > CurrentLevelPointer[0] (ie: the whole level has already scrolled on the screen) then exit
		;Note: This is braindead..
		rep #$30
		lda $CurrentLevelPointer
		sta 0,s
		ldy #$0000
		lda #$0000
		sep #$20
		lda (0,s),y
		sec
		sbc #$07
		cmp ScreenTopLevelRow
		
		bne DoBG1Scroll2
		lda #$01
		bra ScrollBG1TearDown
		
DoBG1Scroll2:
		;So, at (ScreenTopRow + 7) % 8 draw (ScreenTopLevelRow + 7)
		lda ScreenTopLevelRow
		clc
		adc #$07

		pha
		
		lda ScreenTopRow
		sec
		dec
		and #$07

		pha

		jsr DrawLevelRow

		pla : pla

		lda #$00	;Return value -- more to scroll

	;Tear Down	
ScrollBG1TearDown:
	;Release the 2 bytes of stack
	tax
	rep #$30
	tsc
	clc
	adc #$02
	tcs
	lda #$0000
	sep #$20
	txa

	plp
	ply
	plx
	rts
;}}}


DrawInitialScreen:
;{{{
	; Purpose: On a restart, redraw the screen
	;
	; Assumptions: Index registers are 16-bit and accumulator is 8-bit
	
	nop
	nop
	pha
	phx
	php

	;Give us 4 bytes of stack
	rep #$30
	tsc
	sec
	sbc #$04
	tcs
	lda #$0000	;The high byte of A should become 0 on the rep below, but there's no guarantee
	sep #$20
	
	;Set scroll to 0
	lda #$ff
	sta $210e
	stz $210e
	sta AmountBG1Scrolled
	
	stz ScreenTopRow		;Top row of the screen now is 0
	
	;Now starting with ScreenTopLevelRow, repaint the screen rows
	ldx #$0000
	lda ScreenTopLevelRow
	sta 3,s
-
	txa
	sta 1,s		;Store the screen row parameter
	
	lda 3,s
	sta 2,s
	inc
	sta 3,s		;Store the level row parameter

	jsr DrawLevelRow

	inx
	sec
	cpx #$8
	bne -

	;Set the new ball position
	lda #(32 * 4)
	sta BallXPos
	lda #16
	sta BallYPos

	;Start in an invulnerable state
	lda #$01
	sta BallState

	;Release the 4 bytes of stack
	rep #$30
	tsc
	clc
	adc #$04
	tcs
	sep #$20
	
	plp
	plx
	pla
	rts
;}}}


IsBallDead:
;{{{
	; Purpose: Returns (in A) 0 if ball is over a solid tile, or 1 if not
	; Assumptions: X,Y are 16-bit, A is 8-bit
	;
	; Variables:
	;
	; 0,s - Pointer to the tile the ball is on
	; 1,s - The row the ball is on
	; 2,s - Ball mid-y pos (midy)
	; 3,s - Ball mid-x pos
	; 4,s - Tile column ball is in

	nop
	nop

	phx		;16bits
	phy		;16bits
	php		;8bits

	;Give us 5 bytes of stack
	rep #$30
	tsc
	sec
	sbc #$05
	tcs
	lda #$0000	;The high byte of A should become 0 on the rep below, but there's no guarantee
	sep #$20

	;First see if we're invulnerable or not
	lda BallState
	sec
	cmp #$01
	beq IsBallDeadRet0

	
	;Get the midpoints
	lda BallXPos
	clc
	adc #$08
	sta 3,s
	lda BallYPos		;Since we're using a chocobo now, use the very bottom of the feet, not the midpoint
	clc
	adc #$10
	sta 2,s

	;Determine the tile column the ball is in
	;If midx < 64 then dead
	lda 3,s
	cmp #64
	bmi IsBallDeadRet1

	;If midx < 64 + 32*1 then ball is in column 0
	lda #0
	sta 4,s
	lda 3,s
	cmp #(64 + 32*1)
	bmi BallXPosOk

	;If midx < 64 + 32*2 then ball is in column 1
	lda #1
	sta 4,s
	lda 3,s
	cmp #(64 + 32*2)
	bmi BallXPosOk

	;If midx < 64 + 32*3 then ball is in column 2
	lda #2
	sta 4,s
	lda 3,s
	cmp #(64 + 32*3)
	bmi BallXPosOk
	
	;If midx < 64 + 32*4 then ball is in column 3
	lda #3
	sta 4,s
	lda 3,s
	cmp #(64 + 32*4)
	bmi BallXPosOk

	;Otherwise, if we get here, the ball is too far to the right, and so dead
	bra IsBallDeadRet1
	

BallXPosOk:
	;Ok, so now we want to eventually get a row offset into the level data
	;So, say dy = (AmountBG1Scrolled+1) % 32, then our row offset is
	;	rowoff = [floor((midy + dy) / 32) + ScreenTopLevelRow] * 4

	;Get dy
	nop
	lda AmountBG1Scrolled
	inc
	and #31
	sta 1,s

	;midY + dy
	lda 2,s
	clc
	adc 1,s

	;floor((midy + dy) / 32)
	lsr : lsr : lsr : lsr : lsr

	;[.. + ScreenTopLevelRow] * 4
	clc
	adc ScreenTopLevelRow
	asl : asl
	
	ldy #$0000		;Y will be our rowoff
	tay	
	
	;Check if tile we're on is empty or not - very hackish..
	lda #$00
	sta 1,s			;Yes, we just blew away 1,s, but that's ok since we have it (sorta) in Y
	lda 4,s
	sta 0,s		
	
	rep #$30
	lda $CurrentLevelPointer
	inc				;Skip over (# of lines) byte
	clc	
	adc 0,s
	
	sta 0,s
	lda #$0000

	sep #$20		;A is 8-bit again..
	lda (0,s),y
	
	sec
	cmp #$20
	beq IsBallDeadRet1
	
IsBallDeadRet0:
	lda #$00
	bra IsBallDeadDone

IsBallDeadRet1:
	lda #$01

IsBallDeadDone:
	;Release the 5 bytes of stack
	tax
	rep #$30
	tsc
	clc
	adc #$05
	tcs
	lda #$0000
	sep #$20
	txa
	
	;Restore state and return
	plp
	ply
	plx

	rts
;}}}


UpdateBallSprite:
;{{{
	pha		;8bits
	phx		;16bits
	phy		;16bits
	php		;8bits



	lda #$06		;FIXME: Why is this 6? It should be 0!

	sta $2102
	stz $2103
	
	lda BallXPos
	sta $2104
	lda BallYPos
	sta $2104

	;Figure out what image to show based upon the ball state
	lda TickCounter		;Set which animation step should be shown (easy with two frames per direction)
	and #$08
	lsr : lsr
	clc
	
	adc SpriteTileOffset			;Set tile offset based on direction
	
	sta $2104

	;Set the palette to use -- hacky
	lda BallState
	asl
	clc
	adc #%00100000	; Set priority
	
	sta $2104
	
	;Restore state and return
	plp
	ply
	plx
	pla

	rts
;}}}


UpdateBallPosition:
;{{{
	pha

	;Read in joypad info
	
	;Wait for joypad data to be ready
-
	lda $4212
	and #$01
	bne -

	;Read in data
	lda $4219	;(pad data is in 4219)

	stz SpriteTileOffset
	
	;Now update the ball position
	lsr				;Right
	bcc UBP1
	inc BallXPos
	inc BallXPos
	pha
	lda #12
	sta SpriteTileOffset

	lda BG2ScrollAmountX
	inc
	inc
	sta BG2ScrollAmountX
	sta $210f
	
	pla

UBP1:
	lsr				;Left
	bcc UBP2
	dec BallXPos
	dec BallXPos
	pha
	lda #8
	sta SpriteTileOffset

	lda BG2ScrollAmountX
	dec
	dec
	sta BG2ScrollAmountX
	sta $210f
	
	pla

	

UBP2:
	lsr				;Down
	bcc UBP3
	inc BallYPos
	inc BallYPos
	pha
	lda #0
	sta SpriteTileOffset

	lda BG2ScrollAmountY
	inc
	inc
	sta BG2ScrollAmountY
	sta $2110
	
	pla

	
UBP3:
	lsr				;Up
	bcc UBP4
	dec BallYPos
	dec BallYPos
	pha
	lda #4
	sta SpriteTileOffset

	lda BG2ScrollAmountY
	dec
	dec
	sta BG2ScrollAmountY
	sta $2110
	
	pla

	
UBP4:
	lsr				;Start button

	lsr				;Select
	bcc UBP4a

	stz Scratch
	jsr HandlePause

UBP4a:
		lsr 		;Y
		;Y button, which we'll say toggles invulnerability
		bcc UBP4b

		;Check to see if Y isn't being held down
		lda LastYState
		sec
		cmp #$01
		beq UBP4c
	
		lda BallState	;toggle ball state
		inc
		and #$01
		sta BallState
		lda #$01
		sta LastYState
		bra UBP4c


UBP4b:
	stz LastYState		;Indicate that the Y button isn't pressed

UBP4c:
	;Make sure the Y pos isn't too little or too large
	lda BallYPos
	and #%11111100
	bne UBP5
	
	lda #4
	sta BallYPos

UBP5:
	lda BallYPos
	and #%10000000
	beq UBP6

	lda BallYPos
	sec
	cmp #204
	bmi UBP6
	
	lda #204
	sta BallYPos

UBP6:
	;Likewise, ensure X position isn't too big or too small
	lda BallXPos
	sec
	cmp #(2*32 - 20)
	bne UBP7
	lda #(2*32 - 20 + 2)
	sta BallXPos

UBP7:
	lda BallXPos
	sec
	cmp #(6*32 + 10)
	bne UBP8
	lda #(6*32 + 10 - 2)
	sta BallXPos

UBP8:
	pla
	rts
;}}}


DrawLevelRow: 
;{{{
	;	Purpose: Display the given row of four tiles on the screen at a particular spot :)
	;	Assumptions: Upon entering index regs are 16bit and A is 8bit (they will be so upon leaving)
	;

	;	Args:
	;	(15),s : levelRow (8bit)	-- The row in the level data to display
	;	(16),s : screenRow (8bit)	-- The row on the screen to display at (rows being 32 pixels high)
	;
	;	Local variables:
	;	(2),s : Current screen map pos (for start of line)
	;	(4),s : Pointer to the current level data

	pha		;8bits
	phx		;16bits
	phy		;16bits
	php		;8bits

	;Give us 6 bytes of stack
	rep #$30
	tsc
	sec
	sbc #$06
	tcs
	lda #$0000	;The high byte of A should become 0 on the rep below, but there's no guarantee
	sep #$20

	;First get the appropriate screen pos to start at and set it in $2116
	lda 15,s

	rep #$30	;Make A now 16-bit
	;Screen pos is BG1ScreenDataAddr + ScreenRow*32*4 + 8
	clc
	asl : asl : asl : asl : asl : asl : asl : adc #8	;Multiply a by 32*4 and add 8
	clc
	adc #BG1ScreenDataAddr					;And add in the screen data offset
	sta $2116
	sta 4,s

	;Now get a pointer to the levelRow (store in Y)
	lda #$0000
	sep #$20			;Make A 8-bit for a sec
	lda 16,s
	rep #$30			;Make A 16-bit again
	asl : asl : inc		;Multiply a by 4 and add 1 (skip over level row count and
						;jump to appropriate row)
	tay
	
	;Store CurrentLevelPointer on the stack
	lda $CurrentLevelPointer
	sta 2,s

	;Pseudo-code for what I'm about to do (more or less)		-- lies.. all lies..
	;for(z=0; z<4; z++) {
	;	for(i=0; i<4; i++) {
	;		for(j=0; j<4; j++) {
	;			Write (levelRow[i]) to $2118 (and $00 to $2119) 
	;		}
	;	}
	;	screenRow++;
	;}
	
	ldx #$0000
	lda #$0000

	;Determine the proximity bytes
		;Give us another 4 bytes of stack -- ugh..
		rep #$30
		tsc
		sec
		sbc #$04
		tcs
		lda #$0000	;The high byte of A should become 0 on the rep below, but there's no guarantee
		sep #$20
		jsr DetermineProxAdj	;Y holds the offset into the level data
		;Release the 4 bytes of stack
		tsc
		clc
		adc #$04
		tcs
		sep #$20
		
DrawLevelRowLoop:	;Y is index into level data
	;Do top row
	ldx #$0000
DLRL1:
	lda (2,s),y
	sec
	sbc #$20
	bne TileNotEmptyTop

	stz $2118 : stz $2119 : stz $2118 : stz $2119 : stz $2118 : stz $2119 : stz $2118 : stz $2119	;Empty tile
	bra TopTileDrawEnd
	
TileNotEmptyTop:
	;X contains index into proxAdj
	jsr DrawTopRowTile

TopTileDrawEnd:
	iny
	inx
	cpx #4
	bne DLRL1
	
	stz Scratch		;We'll use Scratch to count the rows we've done
DrawMiddleTwo:
	;Now move the screen pointer up by 32
	lda 4,s
	clc
	adc #32
	sta $2116
	sta 4,s
	
	;Do two middle rows
	dey : dey : dey : dey
	ldx #$0000
DMT1:
	lda (2,s),y
	
	sec
	sbc #$20
	bne TileNotEmptyMiddle

	stz $2118 : stz $2119 : stz $2118 : stz $2119 : stz $2118 : stz $2119 : stz $2118 : stz $2119	;Empty tile
	bra MiddleTileDrawEnd
	
TileNotEmptyMiddle:
	;X contains index into proxAdj
	jsr DrawMiddleRowTile
	
MiddleTileDrawEnd:
	iny
	inx
	cpx #4
	bne DMT1

	;Make sure we do second row..
	lda Scratch
	bne DrawBottomRow
	inc Scratch
	jmp DrawMiddleTwo	

DrawBottomRow:
	;Do bottom row
	;Now move the screen pointer up by 32
	lda 4,s
	clc
	adc #32
	sta $2116
	sta 4,s

	dey : dey : dey : dey
	ldx #$0000
DBR1:
	lda (2,s),y
	
	sec
	sbc #$20
	bne TileNotEmptyBottom

	stz $2118 : stz $2119 : stz $2118 : stz $2119 : stz $2118 : stz $2119 : stz $2118 : stz $2119	;Empty tile
	bra BottomTileDrawEnd
	
TileNotEmptyBottom:
	;X contains index into proxAdj
	jsr DrawBottomRowTile

BottomTileDrawEnd:
	iny
	inx
	cpx #4
	bne DBR1


DrawLevelRowDone:
	;Release the 6 bytes of stack
	tsc
	clc
	adc #$06
	tcs
	sep #$20
	
	;Restore state and return
	plp
	ply
	plx
	pla

	rts

CornerTileTable:	.dcb 2,5,2,5,4,3,4,1	;Used in drawing the corner tiles
;}}}


DrawTopRowTile:
;{{{
	;Note: This reads in corner proximity bytes and converts them as follows:
	;  1  2  4       2  1  2
	;  8    10  -->  4     4
	; 20 40 80       2  1  2
	;0,s:	proxAdj pointer
	;2,s:	CornerTileTable pointer
	;4,s:	Scratch
	
	php
	pha
	phx
	phy

	phx
	phx
	phx

	txy		;We want to use X to index into proxAdj array

	;Store pointer to proxAdj
	rep #$30
	lda #proxAdj
	sta 0,s

	lda #CornerTileTable
	sta 2,s
	
	lda #$0000
	sep #$20
	
	;Do upper left corner
	lda (0,s),y : and #$08 : lsr : sta 4,s
	lda (0,s),y : and #$01 : asl : adc 4,s : sta 4,s
	lda (0,s),y : and #$02 : lsr : adc 4,s : sta 4,s
	tay				;High byte is 0
	lda (2,s),y
	sta $2118
	stz $2119

	;Do two middle top tiles
	txy
	lda (0,s),y : and #$02
	beq DTRT1
	
	lda #$1		;Something above, so empty tile
	bra DTRT2

DTRT1:
	lda #$04	;Nothing above, so bar

DTRT2:
	sta $2118 : stz $2119 : sta $2118 : stz $2119
	
	;Do upper right corner
	txy		;We need X index again
	lda (0,s),y : and #$02 : lsr : sta 4,s
	lda (0,s),y : and #$04 : lsr : adc 4,s : sta 4,s
	lda (0,s),y : and #$10 
	beq DTRT3
	lda #$4
	
DTRT3:
	adc 4,s : sta 4,s
	tay				;High byte is 0
	lda (2,s),y
	sta $2118
	lda #%01000000
	sta $2119

	plx
	plx
	plx
			
	ply
	plx
	pla
	plp		


	rts
;}}}


DrawMiddleRowTile:
;{{{
	php
	pha
	phx
	phy


	phx
	phx
	phx

	nop
	nop
	
	txy		;We want to use X to index into proxAdj array

	;Store pointer to proxAdj
	rep #$30
	lda #proxAdj
	sta 0,s
	lda #$0000
	sep #$20

	lda (0,s),y
	and #$8
	bne DMRT2

	;Draw bar
	lda #$05
	sta $2118
	stz $2119
	bra DMRT3

DMRT2:
	;Draw blank
	lda #$01
	sta $2118
	stz $2119

DMRT3:
	;Draw two blanks
	lda #$01
	sta $2118
	stz $2119
	sta $2118
	stz $2119

	;Now the final tile
	lda (0,s),y
	and #$10
	beq DMRT4

	;Draw blank
	lda #$01
	sta $2118
	stz $2119
	bra DMRT5

DMRT4:
	;Draw bar (with H flip)
	lda #$05
	sta $2118
	lda #%01000000
	sta $2119


DMRT5:

	plx
	plx
	plx
			

	ply
	plx
	pla
	plp

	rts
;}}}


DrawBottomRowTile:
;{{{
	;Note: This reads in corner proximity bytes and converts them as follows:
	;  1  2  4       2  1  2
	;  8    10  -->  4     4
	; 20 40 80       2  1  2
	;0,s:	proxAdj pointer
	;2,s:	CornerTileTable pointer
	;4,s:	Scratch
	
	php
	pha
	phx
	phy

	phx
	phx
	phx

	txy		;We want to use X to index into proxAdj array

	;Store pointer to proxAdj
	rep #$30
	lda #proxAdj
	sta 0,s

	lda #CornerTileTable
	sta 2,s
	
	lda #$0000
	sep #$20
	
	;Do lower left corner
	lda (0,s),y : and #$08 : lsr : sta 4,s
	
	lda (0,s),y : and #$20 
	beq DBRTa
	lda #$02
DBRTa:
	adc 4,s : sta 4,s
	
	lda (0,s),y : and #$40
	beq DBRTb
	lda #$01
DBRTb:
	adc 4,s : sta 4,s
	
	tay				;High byte is 0
	lda (2,s),y
	sta $2118
	lda #%10000000
	sta $2119

	;Do two middle top tiles
	txy
	lda (0,s),y : and #$40
	beq DBRT1
	
	lda #$1		;Something below, so empty tile
	bra DBRT2

DBRT1:
	lda #$04	;Nothing below, so bar

DBRT2:
	tay
	sta $2118 : lda #%10000000 : sta $2119 : tya
	sta $2118 : lda #%10000000 : sta $2119 : tya
	

	;Do lower right corner
	txy
	lda (0,s),y : and #$10
	beq DBRTe
	lda #$04
DBRTe:
	sta 4,s
	
	lda (0,s),y : and #$80
	beq DBRTf
	lda #$02
DBRTf:
	adc 4,s : sta 4,s
	
	lda (0,s),y : and #$40
	beq DBRTg
	lda #$01
DBRTg:
	adc 4,s : sta 4,s
	
	tay				;High byte is 0
	lda (2,s),y
	sta $2118
	lda #%11000000
	sta $2119


	plx
	plx
	plx
			
	ply
	plx
	pla
	plp		


	rts
;}}}

DetermineProxAdj:
;{{{
CLPOffset		=	20
ProxAdjOffset	=	03
	;Purpose: Determine proximity array for current level row
	;			This array tells us for each tile in the current row, what tiles are adjacent to it.
	;			This was painfully done by hand. Would have been more elegant to use loops and whatnot,
	;			but this was quicker to code.
	;
	;	1   2   4
	;   8      10
	;  20  40  80
	;
	;Note: Y contains offset from CurrentLevelPointer to level data
	;
	;Variables:
	; CLPOffset,s:		CurrentLevelPointer
	; ProxAdjOffset,s:		Pointer to proxAdj
	; 0,s:		Scratch (short term)

	
	php
	pha
	phx
	phy

	;Give some stack
	phx
	phx
	phx

	nop
	nop
	
	;Store pointer to proxAdj
	rep #$30
	lda #proxAdj
	sta ProxAdjOffset,s
	lda #$0000
	sep #$20
	
	;Empty out proxAdj
	phy
	ldy #$0000
	lda #$00
	sta (ProxAdjOffset+2,s),y
	iny : sta (ProxAdjOffset+2,s),y
	iny : sta (ProxAdjOffset+2,s),y
	iny : sta (ProxAdjOffset+2,s),y
	ply

	;Do adjacency for above (if not the first row)
	sec
	cpy #$1		;1 would be the first level data byte
	beq DPA01
	bra DPA02

DPA01:
	dey			;Decrement Y since first thing that'll happen after the jump is iny
	jmp FB4

DPA02:
	
	dey : dey : dey : dey		;Point to previous row
	ldx #$0000		;Index to proxAdj
	
		;Do first level byte  (nothing to the left)
		;{{{
		lda (CLPOffset,s),y	;Check for something above
		sec
		cmp #$20
		beq FB1
	
		lda #$2		;Set top bit for first tile
		phy : phx : ply : sta (ProxAdjOffset+2,s),y : ply

		inx
		lda #$1		;Set top left bit for second level byte
		phy : phx : ply : sta (ProxAdjOffset+2,s),y : ply
		dex
FB1:
		;Y is -4, X is 0
		iny
		lda (CLPOffset,s),y		;Check for something above right
		sec
		cmp #$20
		beq FB2
	
		lda #$4		;Add top right bit for first tile
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		inx			;Add up bit for second level byte
		lda #$02
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		inx			;Set up left bit for third level byte
		lda #$01
		phy : phx : ply : sta (ProxAdjOffset+2,s),y : ply

		dex
		dex	
FB2:
		;Y is -3, X is 0
		iny			;Move on to third above byte
		inx			;First level byte no longer affected

		lda (CLPOffset,s),y		;Check for something above right
		sec
		cmp #$20
		beq FB3

		lda #$4			;Add top right bit for second level byte
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		inx
		lda #$2			;Add top bit for third level byte
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply
		
		inx
		lda #$1			;Set top left bit for fourth level byte
		phy : phx : ply : sta (ProxAdjOffset+2,s),y : ply

		dex
		dex
FB3:
		;Y is -2, X is 1
		iny			;Move on to fourth above byte
		inx			;Second level byte no longer affected

		lda (CLPOffset,s),y
		sec
		cmp #$20
		beq FB4
		
		lda #$4		;Add top right bit for third level byte
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply
		
		inx
		lda #$02	;Add top but for fourth level byte
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply : dex
;}}}
		
FB4:
		;Now, we do left/right.. reset Y and X
		;{{{
		iny			;Point to current level row
		ldx #$0000

		lda (CLPOffset,s),y	;Is the first tile there?
		sec
		cmp #$20
		beq SB5

		lda #$08		;Add left bit to second tile
		inx : phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply : dex

SB5:
		;At this point X is still 0 and Y is pointing to first level row tile
		nop
		nop

		iny
		lda (CLPOffset,s),y		;Is the second tile there?		
		sec
		cmp #$20
		beq SB6

		lda #$10		;Add right bit to first tile byte
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		inx : inx
		lda #$08		;Add left bit to third tile byte
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply : dex : dex

SB6:
		;Y is pointing to second level row tile
		;X is still 0
		inx
		iny
		lda (CLPOffset,s),y	;Is the third tile there?
		sec
		cmp #$20
		beq SB7

		lda #$10		;Add right bit to second tile
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		inx : inx
		lda #$08		;Add left bit to fourth tile
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply : dex : dex

SB7:
		;X is 1 (second tile)
		;Y is third tile
		iny
		inx
		lda (CLPOffset,s),y	;Is the fourth tile there?
		sec
		cmp #$20
		beq SB8

		lda #$10		;Add right bit to third tile
		phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply
;}}}

SB8:	;Do bottom row
;{{{
		;Y is fourth tile
		iny		;first tile, lower row
		ldx #$0000

		;We won't bother doing this if this is the last row
		phy
		ldy #$0000
		lda (CLPOffset+2,s),y
		ply
		rep #$30
		and #$ff
		asl : asl
		;If Y == A then we skip this part
		sta 0,s : tya : dec : sec : sbc 0,s
		sep #$20
		bne BR1 : jmp DetermineProxAdjDone

BR1:
		lda (CLPOffset,s),y		;Get first bottom row byte
		sec
		cmp #$20
		beq BR2

		lda #$40		;Add bottom bit to first tile
		phy : phx : ply :clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		lda #$20		;Add bottom left bit to second tile
		inx : phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply : dex

BR2:
		;X is 0, Y is first tile, lower row
		iny
		lda (CLPOffset,s),y		;Get second bottom row byte
		sec
		cmp #$20
		beq BR3

		lda #$80		;Add bottom right bit to first tile
		phy : phx : ply :clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		lda #$40		;Add bottom bit to second byte
		inx : phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		lda #$20		;Add bottom left bit to third byte
		inx : phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		dex
		dex

BR3:
		;X is 0, Y is second tile, lower row
		inx : iny
		lda (CLPOffset,s),y		;Get third bottom row byte
		sec
		cmp #$20
		beq BR4

		lda #$80		;Add bottom right bit to second byte
		phy : phx : ply :clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		lda #$40		;Add bottom bit to third byte
		inx : phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		lda #$20		;Add bottom left bit to fourth byte
		inx : phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		dex
		dex

BR4:
		;X is 1, Y is third tile, lower row
		inx : iny
		lda (CLPOffset,s),y		;Get fourth bottom row byte
		sec
		cmp #$20
		beq DetermineProxAdjDone

		lda #$80		;Add bottom right bit to third byte
		phy : phx : ply :clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply

		lda #$40		;Add bottom bit to fourth byte
		inx : phy : phx : ply : clc
		adc (ProxAdjOffset+2,s),y
		sta (ProxAdjOffset+2,s),y
		ply
		
;}}}



DetermineProxAdjDone:
	nop
	nop

	plx
	plx
	plx

	ply
	plx
	pla
	plp

	rts
;}}}








; vim:ts=4:sw=4:ft=asm:norl:ff=dos
; vim600: set commentstring=;%s: set foldmethod=marker:
