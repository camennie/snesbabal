;Data maps and stuff for babal



;ROM header definitions 
	;{{{
	.interrupts
		reset = restart
	.end

	restart         jmp $008000

	.cartridge
		title           =       "SNES Babal"
		mode            =       $20				;LoROM mode
		type            =       $01             ;SRAM, no special chips
		romsize         =       $08             ;32k ROM (256kbit)
		ramsize         =       $00             ;0k SRAM
		country         =       $01				;USA
		maker           =       $00				;Not set (use $13 for RSR)
		version         =       $03				;Revision of ROM
	.end
;}}}


;Global data
;{{{
.base $0000
CurrentLevelPointer:	.dcb	$00, $00	;Pointer to the current level data
ScreenTopLevelRow:		.dcb	$00			;The level row that's currently at the top of the screen (and scrolling by)
ScreenTopRow:			.dcb	$00			;The screen row that's at the top of the screen :)
AmountBG1Scrolled:		.dcb	$00			;The degree to which BG1 has been scrolled up
BG2ScrollAmountX		.dcb	$00			;Amount BG2 has been scrolled horizontally
BG2ScrollAmountY		.dcb	$00			;Amount BG2 has been scrolled vertically

BallXPos:				.dcb	$00			;The X pos of the ball
BallYPos:				.dcw	$0000		;The Y pos of the ball (16bit on purpose)
SpriteTileOffset:		.dcb	$00			;Don't think this is relevant anymore.. 

CurrentLevel:			.dcb	$00			;The current level we're playing
LevelPointerTable:		.dcw	$0000, $0000, $0000, $0000, $0000	;Table of pointers to all the levels

BallState:				.dcb	$00			;0 if ball is normal, of 1 if invulnerable
LastYState:				.dcb	$00			;The state of the Y button when we last handled it (so we can figure out if its been unpressed since last push, etc)

CurrentScore:			.dcw	$0000		;The player's score
TimeLeft:				.dcw	$0000		;Seconds left to complete level
MagicLeft:				.dcw	$0000		;Amount of invulnerability magic left
TickCounter:			.dcb	$00			;Count the number of screen refreshes.. used as a simple timer thingy

Scratch:				.dcw	$0000		;Quick scratch
Scratch1:				.dcw	$0000
Scratch2:				.dcw	$0000

;Stuff for division routine
divisor:				.dcw $0000
dividend:				.dcw $0000
divResult:				.dcw $0000
divRemainder:			.dcw $0000

;Stuff for display number routine
DNZeroPad: 				.dcw $0000

;Stuff for rendering the tiles
proxAdj:				.dcw $0000, $0000	;Adjacency bytes for the four corners (see tile stuff for detail)
BG1Flip:				.dcb $00			;Byte giving the tile flip properties
levelRow:				.dcw $0000, $0000	;The current level row (useful? perhaps...)



.end
	;}}}


;Tile Maps and palettes
;{{{
BG1TilePalette:
.incbin "edges.col"

BG1TileMap: 
.incbin "edges.set"

BG2TilePalette:
.incbin "stone.col"

BG2TileMap:
.incbin "stone.set"

BG2Screen:
.incbin "stone.map"

BallSpritePalette:
.incbin "chocobo.col"

BallSpriteData:
.incbin "chocobo.set"
;}}}


;Font taken from Yoshi tutorial and blw demo
;============================================================================
;= Cyber Font-Editor V1.4  Rel. by Frantic (c) 1991-1992 Sanity Productions =
;============================================================================
BG3Font1:
;{{{
		.dcb	$00,$00,$00,$00,$00,$00,$00,$00 ;'@'
	.dcb	$0c,$3e,$36,$66,$7e,$c6,$c6,$00	;'A'
	.dcb	$fc,$fe,$06,$fc,$c6,$fe,$fc,$00	;'B'
	.dcb	$7c,$fe,$c6,$c0,$c6,$fe,$7c,$00	;'C'
	.dcb	$fc,$fe,$06,$c6,$c6,$fe,$fc,$00	;'D'
	.dcb	$7e,$fe,$c0,$fe,$c0,$fe,$7e,$00	;'E'	
	.dcb	$fe,$fe,$00,$fc,$c0,$c0,$c0,$00	;'F'
	.dcb	$7c,$fe,$c0,$ce,$c6,$fe,$7c,$00	;'G'
	.dcb	$c6,$c6,$c6,$f6,$c6,$c6,$c6,$00	;'H'
	.dcb	$7e,$7e,$18,$18,$18,$7e,$7e,$00	;'I'	
	.dcb	$7e,$7e,$0c,$cc,$cc,$fc,$78,$00	;'J'
	.dcb	$c6,$cc,$d8,$f0,$d8,$cc,$c6,$00	;'K'
	.dcb	$c0,$c0,$c0,$c0,$c0,$fe,$7e,$00	;'L'
	.dcb	$c6,$ee,$fe,$fe,$d6,$c6,$c6,$00	;'M'
	.dcb	$cc,$ec,$fc,$fc,$dc,$cc,$cc,$00	;'N'
	.dcb	$7c,$fe,$c6,$c6,$c6,$fe,$7c,$00	;'O'
	.dcb	$fc,$fe,$06,$fc,$c0,$c0,$c0,$00	;'P'
	.dcb	$7c,$fe,$c6,$c6,$c6,$fe,$7b,$00	;'Q'
	.dcb	$f8,$fe,$06,$fc,$c6,$c6,$c6,$00	;'R'
	.dcb	$7e,$fe,$c0,$7c,$06,$fe,$fc,$00	;'S'
	.dcb	$f8,$fc,$0c,$0c,$0c,$0c,$0c,$00	;'T'
	.dcb	$c6,$c6,$c6,$c6,$c6,$fe,$7c,$00	;'U'
	.dcb	$c6,$c6,$c6,$c6,$ee,$7c,$38,$00	;'V'
	.dcb	$c6,$c6,$d6,$fe,$fe,$ee,$c6,$00	;'W'
	.dcb	$c6,$ee,$7c,$38,$7c,$ee,$c6,$00	;'X'
	.dcb	$66,$66,$66,$3c,$18,$18,$18,$00	;'9'
	.dcb	$fe,$fe,$1c,$38,$70,$fe,$fe,$00	;'Z'
		.dcb	$00,$00,$00,$00,$00,$00,$00,$00	;'['
		.dcb	$00,$00,$00,$00,$00,$00,$00,$00	;'\'
		.dcb	$00,$00,$00,$00,$00,$00,$00,$00	;']'
		.dcb	$00,$00,$00,$00,$00,$00,$00,$00	;'^'
		.dcb	$00,$00,$00,$00,$00,$00,$00,$00	;'_'
	.dcb	$00,$00,$00,$00,$00,$00,$00,$00	;' '
	.dcb	$18,$18,$18,$18,$00,$18,$18,$00	;'!'
	.dcb	$66,$66,$66,$00,$00,$00,$00,$00	;'"'
	.dcb	$6c,$fe,$6c,$6c,$6c,$fe,$6c,$00	;'#'
	.dcb	$10,$7e,$d0,$7c,$16,$fc,$10,$00	;'$'
		.dcb	$00,$00,$00,$00,$00,$00,$00,$00 ;'%'
	.dcb	$78,$cc,$78,$70,$de,$cc,$7e,$00	;'&'
	.dcb	$18,$18,$18,$00,$00,$00,$00,$00	;'''
	.dcb	$18,$30,$60,$60,$60,$30,$18,$00	;'('
	.dcb	$30,$18,$0c,$0c,$0c,$18,$30,$00	;')'
	.dcb	$00,$54,$38,$7c,$38,$54,$00,$00	;'*'
	.dcb	$00,$18,$18,$7e,$7e,$18,$18,$00	;'+'
	.dcb	$00,$00,$00,$00,$00,$18,$18,$30	;','
		.dcb     $00,$00,$00,$00,$00,$00,$00,$00 ;'-'
	.dcb	$00,$00,$00,$00,$00,$18,$18,$00	;'.'
	.dcb	$00,$03,$06,$0c,$18,$30,$60,$00	;'/'
	.dcb	$7c,$fe,$ce,$d6,$e6,$fe,$7c,$00	;'0'
	.dcb	$30,$70,$30,$30,$30,$fc,$fc,$00	;'1'
	.dcb	$fc,$fe,$0e,$3c,$f0,$fe,$fe,$00	;'2'
	.dcb	$fc,$fe,$06,$7c,$06,$fe,$fc,$00	;'3'
	.dcb	$c0,$c0,$cc,$cc,$fe,$fe,$0c,$00	;'4'
	.dcb	$fe,$fe,$c0,$fc,$0e,$fe,$fc,$00	;'5'
	.dcb	$7e,$fe,$c0,$fc,$c6,$fe,$7c,$00	;'6'
	.dcb	$fe,$fe,$0e,$1c,$38,$38,$38,$00	;'7'
	.dcb	$7c,$fe,$c6,$7c,$c6,$fe,$7c,$00	;'8'
	.dcb	$7c,$fe,$c6,$fe,$06,$fe,$7c,$00	;'9'
	.dcb	$00,$30,$30,$00,$30,$30,$00,$00	;':'
	.dcb	$00,$18,$18,$00,$18,$18,$30,$00	;';'
	.dcb	$0e,$18,$30,$60,$30,$18,$0e,$00	;'<'
	.dcb	$00,$00,$7e,$00,$7e,$00,$00,$00	;'='
	.dcb	$70,$18,$0c,$06,$0c,$18,$70,$00	;'>'
	.dcb	$3c,$66,$06,$0c,$18,$00,$18,$00	;'?'
;}}}


;Level data
Level1: 
;{{{
	.dcb	64		;64 rows
	
	.dcb	"## #"	;Level format is: If character isn't a space, it's a solid block, if it is a space, its empty
	.dcb	"  # "
	.dcb    "  ##"
	.dcb    " #  "
	.dcb    " # #"	;5
	.dcb    " ## "
	.dcb    " ###"
	.dcb    "#   "
	.dcb    "#  #"
	.dcb    "# # "	;10
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"
	.dcb    "### "
	.dcb    "####"	;15
	.dcb    "   #"
	.dcb    "  # "
	.dcb    "  ##"
	.dcb    " #  "
	.dcb    " # #"	;20
	.dcb    " ## "
	.dcb    " ###"
	.dcb    "#   "
	.dcb    "#  #"
	.dcb    "# # "	;25
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"
	.dcb    "### "
	.dcb    "####"	;30
	.dcb    "    "
	.dcb    "   #"
	.dcb    "  # "
	.dcb    "  ##"
	.dcb    " #  "	;35
	.dcb    " # #"
	.dcb    " ## "
	.dcb    " ###"
	.dcb    "#   "
	.dcb    "#  #"	;40
	.dcb    "# # "
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"
	.dcb    "### "	;45
	.dcb    "####"
	.dcb    "    "
	.dcb    "   #"
	.dcb    "  # "
	.dcb    "  ##"	;50
	.dcb    " #  "
	.dcb    " # #"
	.dcb    " ## "
	.dcb    " ###"
	.dcb    "#   "	;55
	.dcb    "#  #"
	.dcb    "# # "
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"	;60
	.dcb    "### "
	.dcb    "####"
	.dcb    "# # "
	.dcb    " # #" ;}}}

Level2:
;{{{
	.dcb	64		;64 rows
	
	.dcb	"#  #"	;Level format is: If character isn't a space, it's a solid block, if it is a space, its empty
	.dcb	"#  #"
	.dcb    " ## "
	.dcb    " ## "
	.dcb    "#  #"	;5
	.dcb    "#  #"
	.dcb    "    "
	.dcb    "#   "
	.dcb    "#  #"
	.dcb    "# # "	;10
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"
	.dcb    "### "
	.dcb    "####"	;15
	.dcb    "   #"
	.dcb    "  # "
	.dcb    "  ##"
	.dcb    " #  "
	.dcb    " # #"	;20
	.dcb    " ## "
	.dcb    " ###"
	.dcb    "#   "
	.dcb    "#  #"
	.dcb    "# # "	;25
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"
	.dcb    "### "
	.dcb    "####"	;30
	.dcb    "    "
	.dcb    "   #"
	.dcb    "  # "
	.dcb    "  ##"
	.dcb    " #  "	;35
	.dcb    " # #"
	.dcb    " ## "
	.dcb    " ###"
	.dcb    "#   "
	.dcb    "#  #"	;40
	.dcb    "# # "
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"
	.dcb    "### "	;45
	.dcb    "####"
	.dcb    "    "
	.dcb    "   #"
	.dcb    "  # "
	.dcb    "  ##"	;50
	.dcb    " #  "
	.dcb    " # #"
	.dcb    " ## "
	.dcb    " ###"
	.dcb    "#   "	;55
	.dcb    "#  #"
	.dcb    "# # "
	.dcb    "# ##"
	.dcb    "##  "
	.dcb    "## #"	;60
	.dcb    "### "
	.dcb    "####"
	.dcb    "# # "
	.dcb    " # #" ;}}}



.comment
Level3:
Level4:
Level5:
.end

; vim:ts=4:sw=4:ft=asm:norl:ff=dos
; vim600: set commentstring=;%s: set foldmethod=marker:
