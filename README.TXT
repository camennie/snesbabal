SNES Babal by Chris Mennie (camennie <at> gmail.com)


Development Environment:    Linux / DOS
Development Time:           8 days (4 weekends), all code done by hand
Testing Environment:        Snes9X (X and Win9x versions)
Assembler Used:             X816 assembler v1.12f.19970408 by minus/Ballistics
Other Tools Used:           GIF2SOPT and GIF2SPR2
Editor Used:                Vim 6 (highly recommended for the folding)


Introduction:
    This is a very simple that was originally going to be based on the HP-48 
game Babal. As time went on, I veered from that vision and ended up with this.
    The purpose of the game is to keep the chocobo on the tiles. If you fall
off, the game will delay two seconds, and place you back at the top of the
screen. You have a certain amount of "magic points", which are displayed 
in the lower left hand corner of the screen. You can turn the magic on and
off by pressing Y (and the chocobo will turn a red tinge when the magic is
on). The Select button will toggle game pause.
    Time left to complete the level is in the upper left hand corner. Score
is in the upper right hand corner. The score is based on how close to the
bottom of the screen the chocobo is.
    When a level is finished, the game will pause very briefly, then move on
to the next level. After the last level, the game will restart at the first
level.


How to compile:
    To compile, you simply need to (in DOS) do:
    
        X112f -s babal.asm

    This will create the SMC file. The graphics were originally being done
by hand, but that got too painful, and I ended up using GIF2SOPT and GIF2SPR2
to generate the data. The GIF images I used are included.
    Folding was used extensively in the code, and I highly recommend using
Vim 6 to take advantage of this.
        

Issues:
    - Something is messed up with addressing the chocobo sprite. It works, 
      but what I had to do to display it makes no sense to me.
    - Some emulators won't draw the chocobo correctly.
    - Some emulators won't draw the colour gradient on the text correctly.

    
Credits:
    - Chocobo graphics "borrowed" from FF6
    - Tile graphics are mutated FF6 text boxes
    - Init routines and font data were from other demos/tutorials
    

