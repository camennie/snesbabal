;============================================================================
; InitializeSNES -- "standard" initialization of SNES PPU registers
;----------------------------------------------------------------------------
; In: None
;----------------------------------------------------------------------------
; Out: None
;----------------------------------------------------------------------------
; Modifies: A, X, P
;----------------------------------------------------------------------------
InitializeSNES:
  REP #$30
  SEP #$20
  .mem 8
  .index 16
  LDA #$8F
  STA $2100
  LDA #$80
  STA $2115
  LDA #00
  LDX #$2101
CLRRegLoop:
  STA $0000,X
  INX
  CPX #$210D
  BNE CLRRegLoop
  STA $210E
  STA $210E
  STA $210F
  STA $210F
  STA $2110
  STA $2110
  STA $2111
  STA $2111
  STA $2112
  STA $2112
  STA $2113
  STA $2113
  STA $2114
  STA $2114
  LDX #$2116
CLRRegLoopB:
  STA $0000,X
  INX
  CPX #$2133
  BNE CLRRegLoopB
  LDX #$4200
CLRRegLoopC:
  STA $0000,X
  INX
  CPX #$420D
  BNE CLRRegLoopC

  JSR ClearVRAM      ;Reset VRAM
  JSR ClearPalette   ;Reset colors

  RTS
;============================================================================

;============================================================================
; ClearVRAM -- Sets every byte of VRAM to zero
;----------------------------------------------------------------------------
; In: None
;----------------------------------------------------------------------------
; Out: None
;----------------------------------------------------------------------------
; Modifies: flags
;----------------------------------------------------------------------------
ClearVRAM:
   pha
   phx
   php

   REP #$30
   SEP #$20
   .mem 8
   .index 16
   LDA #$80
   STA $2115         ;Set VRAM port to word access
   LDX #$1809
   STX $4300         ;Set DMA mode to fixed source, WORD to $2118/9
   LDX #$0000
   STX $2116         ;Set VRAM port address to $0000
   STX $0000         ;Set $00:0000 to $0000 (assumes scratchpad ram)
   STX $4302         ;Set source address to $xx:0000
   LDA #$00
   STA $4304         ;Set source bank to $00
   LDX #$8000
   STX $4305         ;Set transfer size to 64k bytes
   LDA #$01
   STA $420B         ;Initiate transfer

   plp
   plx
   pla
   RTS
;============================================================================

;============================================================================
; ClearPalette -- Reset all palette colors to zero
;----------------------------------------------------------------------------
; In: None
;----------------------------------------------------------------------------
; Out: None
;----------------------------------------------------------------------------
; Modifies: flags
;----------------------------------------------------------------------------
ClearPalette:
   PHX
   PHP
   REP #$30
   SEP #$20
   .mem 8
   .index 16
   
   STZ $2121
   LDX #$0100
ClearPaletteLoop:
   STZ $2122
   DEX
   BNE ClearPaletteLoop

   PLP
   PLX
   RTS
;============================================================================
