;BG3 stuff (scores, timers, etc)

SetUpBG3PlayLevel:
;{{{
	;Purpose: Init BG3 for gameplay
	php
	pha
	phx

	;Set normal scroll
	lda #$ff
	sta $2112
	stz $2112

	;Clear screen
	ldx #BG3ScreenDataAddr
	stz $2116

	ldx #$0000
	lda #$20
-
	sta $2118
	stz $2119
	inx
	cpx #1024
	bne -

	plx
	pla
	plp
	
	rts
;}}}

	
UpdateScores:
;{{{
	nop
	nop

	php
	pha
	phx
	phy

	lda TickCounter
	and #$1f
	bne US1

	;Update score
	rep #$30
	lda BallYPos
	lsr : lsr : lsr		;Scale score a bit
	clc
	adc CurrentScore
	sta CurrentScore
	sep #$20

	;Display score on screen
	ldx #(BG3ScreenDataAddr + 32 - 5) 	;Set where we're going to write to
	stx $2116
	ldy #05		;Set the display width (pad with 0's up to this much)
	ldx CurrentScore 	;Number to display

	jsr DisplayNumber

US1:
;	lda TickCounter
;	and #$01
;	bne US2
	jsr UpdateTime
;	bra US3

US2:
	;Only update magic if its on
	lda BallState
	beq US3
	jsr UpdateMagic

US3:
	
	ply
	plx
	pla
	plp	

	rts
	;}}}


UpdateTime:
;{{{
	php
	pha
	phx
	phy

	;Update time left
	rep #$30
	lda #$0000
	sep #$20
	
	lda TickCounter
	tax
	ldy #$60
	jsr Divide

	ldy divRemainder
	cpy #$0000
	bne UT2

	;If we're here, then it's been half a second
	lda TimeLeft
	beq UT2
	dec TimeLeft

UT2:
	;Display time left
	ldx #(BG3ScreenDataAddr)
	stx $2116

	ldy #03
	ldx TimeLeft

	jsr DisplayNumber


	ply
	plx
	pla
	plp
	rts
;}}}

	
UpdateMagic:
;{{{
	php
	pha
	phx
	phy

	;Update magic left
	rep #$30
	lda #$0000
	sep #$20
	
	lda TickCounter
	tax
	ldy #$10
	jsr Divide

	ldy divRemainder
	cpy #$0000
	bne UT2

	;If we're here, then it's been half a second
	lda MagicLeft
	beq UM2
	dec MagicLeft
	bra UM3

UM2:
	;No magic left, we must change state
	stz BallState	

UM3:
	;Display time left
	ldx #(BG3ScreenDataAddr + 32*(27))
	stx $2116

	ldy #03
	ldx #$0000
	ldx MagicLeft

	jsr DisplayNumber


	ply
	plx
	pla
	plp
	rts
;}}}


DisplayNumber:
;{{{
	;Purpose: Display a 16-bit number contained in X, padded by Y 0's (up to 5)
	nop
	nop

		
	php
	pha

	sty DNZeroPad

	;Give ourselves 8 bytes of stack (we'll need at most 7)
	phx
	phx
	phx
	phx
	
	ldy #10	;Divisor
	
	;We do this brute force for all five digits to make things quick and dirty
	jsr Divide
	ldx divResult
	lda divRemainder
	clc
	adc #'0'
	sta 6,s
	jsr Divide
	ldx divResult
	lda divRemainder
	clc
	adc #'0'
	sta 5,s
	jsr Divide
	ldx divResult
	lda divRemainder
	clc
	adc #'0'
	sta 4,s
	jsr Divide
	ldx divResult
	lda divRemainder
	clc
	adc #'0'
	sta 3,s
	jsr Divide
	ldx divResult
	lda divRemainder
	clc
	adc #'0'
	sta 2,s
	

	nop
	nop
	
	;Store stack pointer on stack
	rep #$30
	tsa
	sta 0,s

	;So now we want to start at 2,s + (5 - DNZeroPad) and dump out the bytes to $2118/$2119
	lda 0,s
	clc
	adc #($05 + 2)		;+2 to skip over the stack pointer pointer
	sec
	sbc DNZeroPad
	sta 0,s
	lda #$0000
	sep #$20
	
	ldy #$00
-
	lda (0,s),y
	and #$3f
	sta $2118
	lda #%00110000		;Set to use palette 4 (entry 16 in CGRAM), priority 1
	sta $2119

	iny
	cpy DNZeroPad
	bne -


DNDone:
	;Recover stack
	plx
	plx
	plx
	plx

	;Restore state
	pla
	plp

	rts
;}}}


Divide:
;{{{
	;Purpose: Divide X by Y
	
	php
	phx
	phy
	pha

	rep #$30

	sty	divisor
	stx	dividend
	ldx	#16
	lda	#0
	rol	dividend
lp:
	rol
	cmp	divisor
	bcc	nextbit
	sbc	divisor
	
nextbit:
	rol	dividend
	dex
	bne	lp
;"dividend" now contains the result of the division.
	ldy dividend
	sty divResult
	sta	divRemainder
	
	sep #$20
	
	pla
	ply
	plx
	plp

	rts;}}}

	
;HDMA stuff
SetUpBG3HDMA:
;{{{
	php
	pha
	phx
	phy

	ldx #BG3HDMAPalTable
	stx $4322
	stz $4324
	ldx #BG3HDMAColTable
	stx $4332
	stz $4334

	lda #$21
	sta $4321
	lda #$22
	sta $4331

	lda #%00000000
	sta $4320
	lda #%00000010
	sta $4330

	ply
	plx
	pla
	plp

	rts
;}}}


BG3HDMAPalTable:
;{{{
;There must be a better way...
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17 : .dcb 1, 17
.dcb 0
;}}}


BG3HDMAColTable:
;{{{
;Red gradient
.dcb 1		:	.dcw %0000000000011111
.dcb 1		:	.dcw %0010100101011111
.dcb 1		:	.dcw %0101001010011111
.dcb 1		:	.dcw %0111111111111111
.dcb 1		:	.dcw %0101001010011111
.dcb 1		:	.dcw %0010100101011111
.dcb 1		:	.dcw %0000000000011111
.dcb 1		:	.dcw %0000000000011111

;And because I don't know really how HDMA works..
.dcb 20 : .dcw 0
.dcb 20 : .dcw 0

;Green gradient	(green messages start at line 6 - which is line 48)
.dcb 1		:	.dcw %0000001111100000
.dcb 1		:	.dcw %0010101111101010
.dcb 1		:	.dcw %0101001111110100
.dcb 1		:	.dcw %0111111111111111
.dcb 1		:	.dcw %0101001111110100
.dcb 1		:	.dcw %0010101111101010
.dcb 1		:	.dcw %0000001111100000
.dcb 1		:	.dcw %0000001111100000

.dcb 1		:	.dcw %0000001111100000
.dcb 1		:	.dcw %0010101111101010
.dcb 1		:	.dcw %0101001111110100
.dcb 1		:	.dcw %0111111111111111
.dcb 1		:	.dcw %0101001111110100
.dcb 1		:	.dcw %0010101111101010
.dcb 1		:	.dcw %0000001111100000
.dcb 1		:	.dcw %0000001111100000

;And because I don't know really how HDMA works.
.dcb 20 : .dcw 0
.dcb 20 : .dcw 0
.dcb 20 : .dcw 0
.dcb 20 : .dcw 0
.dcb 20 : .dcw 0
.dcb 20 : .dcw 0
.dcb 20 : .dcw 0
.dcb 12 : .dcw 0

;Blue gradient (blue messages start at line 27 - which is line 216)
.dcb 1		:	.dcw %0111110000000000
.dcb 1		:	.dcw %0111110101001010
.dcb 1		:	.dcw %0111111010010100
.dcb 1		:	.dcw %0111111111111111
.dcb 1		:	.dcw %0111111010010100
.dcb 1		:	.dcw %0111110101001010
.dcb 1		:	.dcw %0111110000000000
.dcb 1		:	.dcw %0111110000000000

;Done
.dcb 0
;}}}


; vim:ts=4:sw=4:ft=asm:norl:ff=dos
; vim600: set commentstring=;%s: set foldmethod=marker:
