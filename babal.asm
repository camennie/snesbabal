	
;Pointer defines
;{{{
BG1TileMapAddr		=	$4000
BG2TileMapAddr		=	$5000
BG3TileMapAddr		=	$6000

BG1ScreenDataAddr	=	$1000
BG2ScreenDataAddr	=	$2000
BG3ScreenDataAddr	=	$3000

BallSpriteDataAddr	=	$0000
;}}}


	.org $8000
	.detect on	;register length autodetection

	
start:
	;Processor startup
;{{{
	sei			;disable interrupts
	
	phk
	plb			;set the data bank to be the same as the program bank
	
	clc
	xce			;native 16 bit mode

	rep	#$30
	sep	#$20	;set XY 16 bit, A 8 bit;}}}


	;Init machine, load data, etc..
	jsr InitBabalWorld

	
BabalMain:
;{{{
	;Go to title page .. select level	-- TODO

		
	stz CurrentLevel
	jsr SetUpCurrentLevelPointer

	;Reset score
	ldx #$0000
	stx CurrentScore
	
BM1:
	;Play level
	jsr PlayLevel

	;If dead then loop
	cmp #$01
	bne BM2
	jsr EndGame
	bra BabalMain

BM2:
	;If not dead then move on to next level
	jsr WinLevel

	;Check to see if the current level pointer is null (gone through all levels)
	nop
	nop
	
	ldx CurrentLevelPointer
	cpx #$0000
	beq BabalMain
	
	;Otherwise go on to next level
	bra BM1
;}}}


PlayLevel:
;{{{
	;Purpose: Play level. Return (in A) 0 if finished on time, 1 if not.
	phx
	phy

	;Some necessary setup
	lda	#$00
	sta ScreenTopLevelRow
	lda #$00
	sta ScreenTopRow
	lda #$ff
	sta AmountBG1Scrolled

	;Update Time and Magic
	jsr SetUpTimeLeft
	jsr SetUpMagicLeft
	
	jsr SetUpBG3PlayLevel
	
	stz TickCounter			;Quick hack to make the next line work
	jsr UpdateScores		;So the scores are there from the start
	
	
	;Flame off
	lda #$00
	sta $2100
	cli
	
	jsr DrawInitialScreen

	;Flame on!
	lda #$0f
	sta $2100
	
LevelLoop:	

	;Enable joypad
	lda #$01
	sta $4200
	
	;Do everything in 16ms..
	jsr WaitForVBlank
	
	;Have we died?
	jsr IsBallDead
	sec
	cmp #$01
	bne LevelNotDeadYet
	
	jsr HandleDeath

LevelNotDeadYet:
	;Check to see if we're out of time..
	lda TimeLeft
	beq LevelOverDead

	;Scroll BG1 and check to see if we've gone through the whole level
	jsr ScrollBG1
	pha
	
	;Note: We do the scroll first, because we get a flicker  if we do it last
	;		-- will probably be a non issue when BG1 tiles are done for real
	;Update ball position if direction pressed
	jsr UpdateBallPosition
	
	;Update ball sprite
	jsr UpdateBallSprite
	
	;Update score, time, etc..
	jsr UpdateScores
		
	pla	
	cmp #$01
	
	bne LevelLoop

	bra LevelOverOk

LevelOverDead:
	lda #$01
	bra LevelOver

LevelOverOk:
	lda #$00
	
LevelOver:
	;We've finished level
	ply
	plx
	
	rts;}}}


HandleDeath:
;{{{
	;Purpose: Ball has died. Pause for a few seconds (letting timer run down) and restart when we left off
	nop
	nop

	php
	phx
	phy
	pha

	ldy #$0000
-
	jsr WaitForVBlank
	jsr UpdateTime
	iny
	sec
	cpy #120			;Timing delay (2 seconds)
	bne -
	
	nop
	nop
	
	;Add some magic for when we restart
	rep #$30
	lda MagicLeft
	clc
	adc #$5
	sta MagicLeft
	sep #$20
	
	lda #$00
	sta $2100
	
	jsr DrawInitialScreen
	lda #$0f
	sta $2100

	pla
	ply
	plx
	plp
	rts;}}}


EndGame:
;{{{
	php
	pha
	phx
	phy

	jsr WaitForVBlank

	;Display message
	ldx #(BG3ScreenDataAddr + 32 * 6 + 2)
	stx $2116

	ldx #$0000
-
	lda EndGameMes,x
	beq EGDone

	and #$3f
	sta $2118
	lda #%00110000		;Set to use palette 4 (entry 16 in CGRAM)
	sta $2119
	
	inx
	bra -
	

EGDone:
	;Delay for five seconds
	ldx #$0000
-
	jsr WaitForVBlank
	inx
	cpx #(60 * 5)
	bne -
	

	ply
	plx
	pla
	plp

	rts
EndGameMes: .dcb "GAME","                            ","OVER"
			.dcb $00
	;}}}


WinLevel:
;{{{
	php
	pha
	phx
	phy

	jsr WaitForVBlank

	;Display message
	ldx #(BG3ScreenDataAddr + 32 * 6)
	stx $2116

	ldx #$0000
-
	lda WinLevelMes,x
	beq WLDone

	and #$3f
	sta $2118
	lda #%00110000		;Set to use palette 4 (entry 16 in CGRAM)
	sta $2119
	
	inx
	bra -
	

WLDone:
	;Delay for five seconds
	ldx #$0000
-
	jsr WaitForVBlank
	inx
	cpx #(60 * 5)
	bne -

	;Before we leave, update the level pointer stuff
	inc CurrentLevel
	jsr SetUpCurrentLevelPointer

	ply
	plx
	pla
	plp

	rts
WinLevelMes: .dcb "LEVEL","                           ","FINISHED"
			 .dcb $00
;}}}


HandlePause:
;{{{
	php
	pha
	phx
	phy

	jsr WaitForVBlank

	;Display message
	ldx #(BG3ScreenDataAddr + 32 * 6 + 1)
	stx $2116

	ldx #$0000
-
	lda PauseGameMes,x
	beq HPDone

	and #$3f
	sta $2118
	lda #%00110000		;Set to use palette 4 (entry 16 in CGRAM)
	sta $2119
	
	inx
	bra -
	

HPDone:
	;Wait for select to be hit again
HPDoneL1:
	;Wait for joypad data to be ready
	lda $4212
	and #$01
	bne HPDoneL1

	;Read in data
	lda $4219	;(pad data is in 4219)

	;Wait until select is unpressed
	lsr : lsr : lsr : lsr : lsr : lsr
	bcs HPDoneL1

	;Now wait for select to be pressed and unpressed again
HPDoneL2:
	;Wait for joypad data to be ready
	lda $4212
	and #$01
	bne HPDoneL2

	;Read in data
	lda $4219	;(pad data is in 4219)

	;Wait until select is unpressed
	lsr : lsr : lsr : lsr : lsr : lsr
	bcc HPDoneL2
	
HPDoneL3:
	;Wait for joypad data to be ready
	lda $4212
	and #$01
	bne HPDoneL3

	;Read in data
	lda $4219	;(pad data is in 4219)

	;Wait until select is unpressed
	lsr : lsr : lsr : lsr : lsr : lsr
	bcs HPDoneL3

	;Erase message
	ldx #(BG3ScreenDataAddr + 32 * 6 + 1)
	stx $2116

	ldx #$0000
-
	lda PauseGameMes,x
	beq HPDoneForReal

	lda #$00	
	sta $2118
	lda #%00010000		;Set to use palette 4 (entry 16 in CGRAM)
	sta $2119
	
	inx
	bra -


HPDoneForReal:
	ply
	plx
	pla
	plp

	rts
PauseGameMes: 	.dcb "PAUSED"
				.dcb $00
	;}}}

			
WaitForVBlank: 
;{{{
	pha

	;Start HDMA rolling
	lda #%00000000
	jsr SetUpBG3HDMA
	lda #%00001100
	sta $420c

							
WaitForVBlankLoop		lda $4210
						and #$80
						beq WaitForVBlankLoop
						inc TickCounter
						pla
						rts
;}}}


.incsrc "babalbg1.asm"
.incsrc "babalbg3.asm"
.incsrc "babinit.asm"
.incsrc "babdata.asm"
	


; vim:ts=4:sw=4:ft=asm:norl:ff=dos
; vim600: set commentstring=;%s: set foldmethod=marker:
