;Init stuff for babal



InitBabalWorld:	
;{{{
	;Init SNES
	jsr InitializeSNES

	;Init for my stuff
	jsr babalInit

	;Load tile map for BG1 
;{{{
	ldx #BG1TileMapAddr
	stx $2116
	ldx #$0000

-
	lda BG1TileMap,x
	sta $2118
	inx
	lda BG1TileMap,x
	sta $2119
	inx

	cpx #512
	bne -
	;}}}
	
	
	;Load tile map for BG2
;{{{
	ldx #BG2TileMapAddr
	stx $2116
	ldx #$0000

-
	lda BG2TileMap,x
	sta $2118
	inx
	lda BG2TileMap,x
	sta $2119
	inx

	cpx #2048
	bne -
	;}}}
	
	
	;Load font data for BG3
;{{{
	ldx #BG3TileMapAddr
	stx $2116
	ldx #$0000
-
	lda BG3Font1,x : sta $2118 : stz $2119 : inx	;Load in first bitplane, and zero out second bitplane
	
	cpx #(64 * 8)	;64 characters
	bne -
;}}}


	;Clear BG3
;{{{
	ldx #BG3ScreenDataAddr
	stx $2116
	ldx #$0000
	lda #32		;32 is space
	and #$3f
-
	sta $2118
	stz $2119

	inx
	cpx #1024
	bne -;}}}

	
	;Load the sprite data
	;{{{
	ldx #BallSpriteDataAddr
	stx $2116
	ldx #$0000
-
	lda BallSpriteData,x	;Bitplane 1
	sta $2118
	inx
	lda BallSpriteData,x	;Bitplane 2
	sta $2119
	inx
	lda BallSpriteData,x	;Bitplane 3
	sta $2118
	inx
	lda BallSpriteData,x	;Bitplane 4
	sta $2119
	
	inx
	cpx #1024
	bne -;}}}
	
	
	;Set up the OAM table
	lda #%00000000;{{{
	sta $2101

	
	lda #$00	;Do sprite 0
	sta $2102
	lda #%00000000
	sta $2103

	ldx #$0000
-	
	lda #00		;X pos
	sta $2104
	lda #224	;Y pos (hidden below visible screen)
	sta $2104
	lda #%00000000
	sta $2104
	lda #%00100000
	sta $2104

	inx
	cpx #128
	bne -;}}}
	
	
	;Set up the small OAM table
	ldx #$0000;{{{
-
	lda #%10000000	;Size 1 (16x16)
	sta $2104

	inx
	cpx #32
	bne -;}}}
	
	
	;Set up palettes	
;{{{
	lda #$00
	sta $2121
	ldx #$0000
-
	lda BG1TilePalette,x
	sta $2122
	inx
	lda BG1TilePalette,x
	sta $2122
	inx
	cpx #32
	bne -


	lda #32
	sta $2121
	ldx #$0000
-
	lda BG2TilePalette,x
	sta $2122
	inx
	lda BG2TilePalette,x
	sta $2122
	inx
	cpx #32
	bne -
	

	;Set the background to black
	lda #$00
	sta $2121	;start at colour 0
	
	lda #00 ;#$46
	sta $2122
	lda #00 ;#$69
	sta $2122
	
	;Set up colours for BG3 to use
	lda #16		;Palette 4 (for BG3)
	sta $2121
	
	stz $2122	;Background/invis black
	stz $2122

	lda #$ff
	sta $2122
	sta $2122	;Main colour white

	
	lda #128	;Start at colour 128 (where the sprite palettes start)
	sta $2121
	ldx #$0000		;Load normal palette
-
	lda BallSpritePalette,x
	sta $2122
	inx
	lda BallSpritePalette,x
	sta $2122
	inx
	cpx #32
	bne -
	
	ldx #$0000		;Load magic palette
-
	lda BallSpritePalette,x
	ora #%01110111	
	sta $2122
	inx
	lda BallSpritePalette,x
	ora #%10111101
	sta $2122
	inx
	cpx #32
	bne -

	
;}}}


	;Draw BG2
;{{{
	ldx #BG2ScreenDataAddr
	stx $2116
	ldx #$0000

-
	lda BG2Screen,x
	sta $2118
	inx
	lda BG2Screen,x
	sta $2119
	inx

	cpx #2048
	bne -
	;}}}

	
	rts	
;}}}

.incsrc "snesinit.asm"
	
;Init stuff specific for Babal (babelInit)
;{{{
babalInit:
	lda #%00001001
	sta $2105		; Use screen mode 1, priority 1

	lda #(>BG1ScreenDataAddr)        ; BG1 screen data (32x32)
	sta $2107
	
	lda #(>BG2ScreenDataAddr)        ; BG2 screen data (32x32)
	sta $2108

	lda #(>BG3ScreenDataAddr)		; BG3 screen data (32x32)
	sta $2109

	lda #((>BG2TileMapAddr) + (>BG1TileMapAddr / 16))	;BG1 and BG2 tile data address in VRAM
	sta $210b

	lda #(>BG3TileMapAddr / 16)		; BG3 tile data is at VRAM $5000
	sta $210c

	lda #%00010111	; BG1 and BG2 and BG3 are main screens (and enabled with sprites)
	sta $212c
	
	lda #%00000000	; No sub screens
	sta $212d

	lda #%00000001	; 3.58Mhz access cycle
	sta $420d
	
	lda #%00110000	;No colour addition/subtraction
	sta $2130
	lda #%00001111	;FIXME is this correct?
	sta $2131
	lda #%00000000
	sta $2132

	lda %1100 0011 ;%11110000 ?
	sta $2125       ; Enable colour and objs in windows 1 and 2

	stz $2133		;Set resolution to 256x224
	
	;Init the global variables
	rep #$30
	pha		;Give us 2 bytes of stack
	lda #LevelPointerTable
	sta 0,s
	ldy #$0000
	
	lda #Level1
	sta (0,s),y

	lda 0,s
	inc
	inc
	sta 0,s
	lda #Level2
	sta (0,s),y
	
	lda 0,s
	inc
	inc
	sta 0,s
	lda #$0000		;A will be 0 since there is no level 3
	sta (0,s),y

.comment
	lda 0,s
	inc
	inc
	sta 0,s
	lda #Level3
	sta (0,s),y

	lda 0,s
	inc
	inc
	sta 0,s
	lda #Level4
	sta (0,s),y
.end
	
	pla
	sep #$20
	lda #$00
	sta CurrentLevel
	jsr SetUpCurrentLevelPointer
	
	lda	#$00
	sta ScreenTopLevelRow
	lda #$00
	sta ScreenTopRow
	lda #$ff
	sta AmountBG1Scrolled

	stz BallState
	stz BallYPos+1
	stz TimeLeft+1
	stz TickCounter
	stz SpriteTileOffset
	
	rts 

	
;}}}
	

SetUpCurrentLevelPointer:
;{{{
	; Purpose: Given the current level, set CurrentLevelPointer to point to the correct level data

	nop
	nop
	
	php
	phy
	phx
	pha
	

	phx		;Give ourselves 2 bytes of stack
	
	rep #$30		;Zero out all A, to get around emulator bugs
	lda #$0000
	sep #$20
	

	lda CurrentLevel
	asl
	ldy #$0000
	tay

	rep #$30
	lda #LevelPointerTable
	sta 0,s

	lda (0,s),y
	sta CurrentLevelPointer

	
	;Return allocated stack
	sep #$20
	plx	

	
	pla
	plx
	ply
	plp
	
	rts
;}}}



; vim:ts=4:sw=4:ft=asm:norl:ff=dos
; vim600: set commentstring=;%s: set foldmethod=marker:
